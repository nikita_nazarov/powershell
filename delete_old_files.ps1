<#
--------------------------------
 Project: Delete old files from windows server share
 Version: 0.0.2
 Name: Nazarov Nikita
 Social: vk.com/doojkee
--------------------------------
#>
chcp 866
$limit = (Get-Date).AddDays(-0) # Дата создания минус дней от текущей даты
$path = "E:\Data_SPB\Photos\"

$data = Get-Date -Format dd.MM.yyyy
$time = Get-Date -Format HH-mm-ss 
$hostname = hostname
$log = New-Item "C:\Scripts\delete_old_files\logs\SPB\Photos\delete_old_photos - $hostname - $data - $time.log" -ItemType file
$log_csv = New-Item "C:\Scripts\delete_old_files\logs\SPB\Photos\delete_old_photos - $hostname - $data - $time.csv" -ItemType file

$dir = "C:\Scripts\delete_old_files\logs\SPB\Photos\"
$latest = Get-ChildItem -Filter *.log -Path $dir | Sort-Object LastAccessTime -Descending | Select-Object -First 1
$latest_csv = Get-ChildItem -Filter *.csv -Path $dir | Sort-Object LastAccessTime -Descending | Select-Object -First 1


"Удаление СТАРЫХ ФОТОГРАФИЙ (СТАРШЕ $limit) из \Data_SPB\PHOTOS\" + [System.Environment]::NewLine + "---------------------------------------------" | out-file -filepath "$dir\$($latest.name)" -Append

# Рекурсивно удаляет все файлы, которые удовлетворяет условию (меньше, чем текущая дата -180 дней или 6 ~месяцев), она проверяет дату создания файла.
# И не учитывает дату модификации файла.
# Имхо - нахер не нужно!

$results = @() #хэш=таблица для хранения результатов
$number = 0 # Счетчик файлов
Get-ChildItem -Path $path -Recurse -Force | Where-Object { !$_.PSIsContainer -and $_.CreationTime -lt $limit} | % {
    try {
        $data6 = ((Get-ACL $_.FullName).Owner)
        Remove-Item -Path $_.fullname -Force -Confirm:$false -ErrorAction Stop        
        
        $data1 = "$($_.creationtime.toString("dd.MM.yyyy"))"
        $data2 = "$($_.creationtime.toString("HH:MM:ss"))"
        $data3 = "$($_.fullname)"
        $data4 = "$([math]::round($_.Length/1MB,1)) Mb"
        $data5 = "$($_.Extension)"
        
        $obj = New-Object PSObject
        $obj | Add-Member "Номер" "№$number"
        $obj | Add-Member "Дата создания файла" $data1
        $obj | Add-Member "Время создания файла" $data2
        $obj | Add-Member "Имя удаленного файла" $data3
        $obj | Add-Member "Владелец удаленного файла" $data6         
        $obj | Add-Member "Размер удаленного файла" $data4
        $obj | Add-Member "Тип удаленного файла" $data5  
        $results += $obj | Select-Object Номер, "Дата создания файла", "Время создания файла", "Имя удаленного файла", "Владелец удаленного файла", "Размер удаленного файла", "Тип удаленного файла"
        $obj | out-file -filepath "$dir\$($latest.name)" -Append        
        $number++
      
    } catch {
        "Не удалось удалить файл: $($_.fullname)" | out-file -filepath "C:\Scripts\delete_old_files\logs\SPB\Photos\error_log.log" -Append       
    }
}  
$results | Where {$_} | Export-Csv -Path "$dir\$($latest_csv.name)" -Delimiter ";" -Encoding UTF8 -NoTypeInformation  

"-----------------------------------------------------------------------------"| out-file -filepath "$dir\$($latest.name)" -Append
"                     -----------------------------                           "| out-file -filepath "$dir\$($latest.name)" -Append
"                     Всего удалено файлов: $number                           "| out-file -filepath "$dir\$($latest.name)" -Append
"                     -----------------------------                           "| out-file -filepath "$dir\$($latest.name)" -Append
"-----------------------------------------------------------------------------"| out-file -filepath "$dir\$($latest.name)" -Append

Clear-Variable data1, data2, data3, data4, data4, data5, number, results
#
# Тестовая функция удаления пустых папок, нужно подумать, нужна ли она? и настроить ее, чтобы не удаляла корневые объекты!
#
#"DELETE EMPTY FOLDERS AFTER DELETING FILES FROM \Data_SPB\PHOTOS\" + [System.Environment]::NewLine + "--------------------------" | out-file -filepath "$dir\$($latest.name)" -Append
# Delete any empty directories left behind after deleting the old files.
#Get-ChildItem -Path $path -Recurse -Force | Where-Object { $_.PSIsContainer -and (Get-ChildItem -Path $_.FullName -Recurse -Force | Where-Object { !$_.PSIsContainer }) -eq $null } | Remove-Item -Force -Recurse | out-file -filepath "$dir\$($latest.name)" -Append


#Get-Content "$dir\delete_old_photos - SPB-FILE01 - 22.04.2018 - 20.20.04.log" | ConvertTo-Html | Out-File -FilePath C:\Scripts\1.html