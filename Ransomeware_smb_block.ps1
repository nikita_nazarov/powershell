<#
--------------------------------
 Project: Block Ransomeware files in windows smb share
 Original file by Tim Buntrock
 Version: 0.0.1
 Name: Nazarov Nikita
 Social: vk.com/doojkee
--------------------------------
#>

$data = Get-Date -Format dd.MM.yyyy
$time = Get-Date -Format HH.mm.ss 
$hostname = hostname

$new_file = New-Item "C:\Scripts\Ransomware_protect\logs\RansomWareBlockSmbLog - $hostname - $data - $time.log" -ItemType file
$dir = "C:\Scripts\Ransomware_protect\logs\"
$latest = Get-ChildItem -Path $dir | Sort-Object LastAccessTime -Descending | Select-Object -First 1


$LogFile = $latest.name
$Events = Get-EventLog -LogName application -Source SRMSVC -After (get-date).AddMinutes(-2) | select ReplacementStrings -Unique

        foreach ($Event in $Events)
            {
                $BadUser = $Event.ReplacementStrings[0]
                $SharePath = $Event.ReplacementStrings[1]
                $Rule = $Event.ReplacementStrings[2]
                $BadFile = $Event.ReplacementStrings[3]
                $Posi = $SharePath.LastIndexOf("\")
                $SharePart = $SharePath.Substring($Posi+1)
                
                $SubinaclCmd = "C:\Scripts\Ransomware_protect\subinacl.exe /verbose=1 /share \\spb-file01.petrovskiy.ru\" + "$SharePart" + "$" + " /deny=" + "$BadUser"
                if ($Rule -match "Anti-Ransomware File Groups")
                    {
                        cmd /c $SubinaclCmd

                        $LogValues = "Date: " + "$data" + " Time: " + "$time " + [System.Environment]::NewLine + "$BadUser" + [System.Environment]::NewLine + "--------------------------"
                        $LogValues | out-file -filepath "$dir\$($latest.name)" -Append                      
                    }
                
                else
                    {
                        exit
                    }
            }
			

